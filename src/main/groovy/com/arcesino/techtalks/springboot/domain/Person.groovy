package com.arcesino.techtalks.springboot.domain

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id

  @NotNull
	String firstName
  @NotNull
	String lastName
  String telephone
}
