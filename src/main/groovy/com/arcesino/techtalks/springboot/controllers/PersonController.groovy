package com.arcesino.techtalks.springboot.controllers

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.ApiOperation;

@RestController
class PersonController {

    @RequestMapping(value="arcesino/companies")
    @ApiOperation(httpMethod = "GET", value = "Get all the companies")
    List<String> getCompanies() {
      ["No companies for now"]
    }

}
