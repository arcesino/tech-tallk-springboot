package com.arcesino.techtalks.springboot.repositories

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.arcesino.techtalks.springboot.domain.Person

@RepositoryRestResource(collectionResourceRel = "people", path = "people")
interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

	List<Person> findByLastName(@Param("name") String name)

}
